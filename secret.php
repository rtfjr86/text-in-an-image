<?php
/**
 * Created by Richard T. Fields Jr.
 */

// Get the input options.
$options = getopt("i:t:o:m:f:p:");

if (array_key_exists('p', $options)) {
    if (is_array($options['p'])) {
        exit("\n" . 'ERROR: The option flag "-p" may only be used once.' . "\n\n");
    }
} else {
    exit("\n".'ERROR: You must specify an operation using the "-p" flag.'."\n\n");
}

if ($options['p'] == 'encode') {
//*****************//
// BEGIN: encoding //
//*****************//

    /* BEGIN: option checking */
    if (array_key_exists('i', $options)) {
        if (is_array($options['i'])) {
            exit("\n".'ERROR: The option flag "-i" may only be used once.'."\n\n");
        }

        if (!file_exists($options['i'])) {
            exit("\n".'ERROR: The given image file does not exist.'."\n\n");
        }

        $image = imagecreatefromjpeg($options['i']);

        if (!$image) {
            exit("\n".'ERROR: There was an error with your chosen input image.'."\n\n");
        }
    } else {
        exit("\n".'ERROR: You must enter an image file using the "-i" flag.'."\n\n");
    }

    if (array_key_exists('t', $options)) {
        if (is_array($options['t'])) {
            exit("\n".'ERROR: The option flag "-t" may only be used once.'."\n\n");
        }

        if ($options['t'] != 'stdin'
            && $options['t'] != 'file'
            && $options['t'] != 'cmdln'
        ) {
            exit("\n".'ERROR: Invalid input type.'."\n\n");
        }
    } else {
        exit("\n".'ERROR: You must choose an input type using the "-t" flag.'."\n\n");
    }

    if (array_key_exists('o', $options)) {
        if (is_array($options['o'])) {
            exit("\n".'ERROR: The option flag "-o" may only be used once.'."\n\n");
        }

        if (!is_writable(dirname($options['o']))) {
            exit("\n".'ERROR: You do not have the necessary permissions to create the specified output file.'."\n\n");
        }
    } else {
        exit("\n".'ERROR: You must choose an output file name using the "-o" flag.'."\n\n");
    }
    /* END: option checking */

    /* BEGIN: get text input */
    $textInput = '';

    if ($options['t'] == 'stdin') {
        $read = array(STDIN);
        $write = null;
        $except = null;

        $readyCount = stream_select($read, $write, $except, 0);

        // Make sure that stdin has input before reading from it.
        if ($readyCount > 0 and $read) {
            $textInput = stream_get_contents(STDIN);
        } else {
            exit("\n".'ERROR: You must pipe a message into stdin.'."\n\n");
        }
    } elseif ($options['t'] == 'cmdln') {
        if (array_key_exists('m', $options)) {
            $textInput = $options['m'];
        } else {
            exit("\n".'ERROR: You must type a message using the "-m" flag.'."\n\n");
        }
    } elseif ($options['t'] == 'file') {
        if (array_key_exists('f', $options)) {
            if (!file_exists($options['f'])) {
                exit("\n".'ERROR: The message file "'.$options['f'].'" does not exist.'."\n\n");
            }

            $textInput = file_get_contents($options['f']);

            if ($textInput === false) {
                exit("\n".'ERROR: There was an error reading "'.$options['f'].'".'."\n\n");
            }
        } else {
            exit("\n".'ERROR: You must specify a message file the "-f" flag.'."\n\n");
        }
    }
    /* END: get text input */

    // Get the image size.
    $imageSize = getimagesize($options['i']);
    $imagePixelSize = $imageSize[0] * $imageSize[1];

    $pixelLocation = array(
        'x' => $imageSize[0] - 1,
        'y' => $imageSize[1] - 1,
    );

    // Get the input length.
    $charInputLength = strlen($textInput);
    $binInputLength = $charInputLength * 8;
    $binTextLength = array_pad(str_split(decbin($binInputLength)), -32, "0");
    $pixelCount = ceil($binInputLength / 3);
    $binStr = '';

    // Ensure that the chosen image is large enough to accommodate the given message.
    if ($pixelCount > $imagePixelSize) {
        exit("\n".'ERROR: The image "'.$options['i'].'" is not large enough to accommodate your message.'."\n\n");
    }

    echo "\n".'Working...'."\n";

    // Convert the input string to a binary string.
    for ($i = 0; $i < $charInputLength; $i++) {
        $hexChar = unpack('H*', $textInput[$i]);
        $binChar = base_convert($hexChar[1], 16, 2);
        $binChar = str_pad($binChar, 8, 0, STR_PAD_LEFT);
        $binStr .= $binChar;
    }

    $binStr = str_split($binStr);

    // Encode the sting length in the first 11 pixels.
    for ($i = 0; $i < 11; $i++) {
        // Get the next 3 bits.
        $one = array_shift($binTextLength);
        $two = array_shift($binTextLength);
        $three = array_shift($binTextLength);

        // Ensure each variable is set.
        $one = $one ? $one : '0';
        $two = $two ? $two : '0';
        $three = $three ? $three : '0';

        // Get the original rgb values in binary.
        $rgb = imagecolorat($image, $pixelLocation['x'], $pixelLocation['y']);
        $r = decbin(($rgb >> 16) & 0xFF);
        $g = decbin(($rgb >> 8) & 0xFF);
        $b = decbin($rgb & 0xFF);

        // Replace the least significant bit with our new value.
        $r = substr_replace($r, $one, -1);
        $g = substr_replace($g, $two, -1);
        $b = substr_replace($b, $three, -1);

        // Update the pixel with the new color value.
        $color = imagecolorallocate($image, bindec($r), bindec($g), bindec($b));
        imagesetpixel($image, $pixelLocation['x'], $pixelLocation['y'], $color);
        $pixelLocation['x']--;

        if ($pixelLocation['x'] == 0) {
            $pixelLocation['x'] = $imageSize[0] - 1;
            $pixelLocation['y']--;
        }
    }

    // Encode the input text in the remaining pixels.
    for ($i = 0; $binStr; $i++) {
        // Get the next 3 bits.
        $one = array_shift($binStr);
        $two = array_shift($binStr);
        $three = array_shift($binStr);

        // Ensure each variable is set.
        $one = $one ? $one : '0';
        $two = $two ? $two : '0';
        $three = $three ? $three : '0';

        // Get the original rgb values in binary.
        $rgb = imagecolorat($image, $pixelLocation['x'], $pixelLocation['y']);
        $r = decbin(($rgb >> 16) & 0xFF);
        $g = decbin(($rgb >> 8) & 0xFF);
        $b = decbin($rgb & 0xFF);

        // Replace the least significant bit with our new value.
        $r = substr_replace($r, $one, -1);
        $g = substr_replace($g, $two, -1);
        $b = substr_replace($b, $three, -1);

        // Update the pixel with the new color value.
        $color = imagecolorallocate($image, bindec($r), bindec($g), bindec($b));
        imagesetpixel($image, $pixelLocation['x'], $pixelLocation['y'], $color);
        $pixelLocation['x']--;

        if ($pixelLocation['x'] == 0) {
            $pixelLocation['x'] = $imageSize[0] - 1;
            $pixelLocation['y']--;
        }
    }

    imagepng($image, $options['o']);

    echo 'Your message was successfully encoded in the PNG file "'.$options['o'].'".'."\n\n";
//***************//
// END: encoding //
//***************//
} elseif ($options['p'] == 'decode') {
//*****************//
// BEGIN: decoding //
//*****************//

    /* BEGIN: option checking */
    if (array_key_exists('i', $options)) {
        if (is_array($options['i'])) {
            exit("\n".'ERROR: The option flag "-i" may only be used once.'."\n\n");
        }

        if (!file_exists($options['i'])) {
            exit("\n".'ERROR: The given image file "'.$options['i'].'" does not exist.'."\n\n");
        }

        $image = imagecreatefrompng($options['i']);

        if (!$image) {
            exit("\n".'ERROR: There was an error with your chosen input image.'."\n\n");
        }
    } else {
        exit("\n".'ERROR: You must enter the image file you wish to decode using the "-i" flag.'."\n\n");
    }
    /* END: option checking */

    echo "\n".'Working...'."\n";

    $imageSize = getimagesize($options['i']);
    $binaryMessage = '';

    $pixelLocation = array(
        'x' => $imageSize[0] - 1,
        'y' => $imageSize[1] - 1,
    );

    // Get the message length.
    for ($i = 0; $i < 11; $i++) {
        $rgb = imagecolorat($image, $pixelLocation['x'], $pixelLocation['y']);
        $r = decbin(($rgb >> 16) & 0xFF);
        $g = decbin(($rgb >> 8) & 0xFF);
        $b = decbin($rgb & 0xFF);

        $binaryMessage .= substr($r, -1);
        $binaryMessage .= substr($g, -1);
        $binaryMessage .= $i < 10 ? substr($b, -1) : '';

        $pixelLocation['x']--;

        if ($pixelLocation['x'] == 0) {
            $pixelLocation['x'] = $imageSize[0] - 1;
            $pixelLocation['y']--;
        }
    }

    // Get message info.
    $messageLength = bindec($binaryMessage);
    $messagePixelCount = $messageLength / 3;
    $binaryMessage = '';

    // Get the message.
    for ($i = 11; $i < 12 + $messagePixelCount; $i++) {
        $rgb = imagecolorat($image, $pixelLocation['x'], $pixelLocation['y']);
        $r = decbin(($rgb >> 16) & 0xFF);
        $g = decbin(($rgb >> 8) & 0xFF);
        $b = decbin($rgb & 0xFF);

        $binaryMessage .= substr($r, -1);
        $binaryMessage .= substr($g, -1);
        $binaryMessage .= substr($b, -1);

        $pixelLocation['x']--;

        if ($pixelLocation['x'] == 0) {
            $pixelLocation['x'] = $imageSize[0] - 1;
            $pixelLocation['y']--;
        }
    }

    // Split the binary string into 8 bit characters.
    $binaryMessageArray = str_split($binaryMessage, 8);
    $textString = '';

    // Create the message string.
    foreach ($binaryMessageArray as $binValue) {
        $textString .= hex2bin(base_convert($binValue, 2, 16));
    }

    echo "\n".$textString."\n\n";

    echo 'Your message was successfully decoded.'."\n\n";
//***************//
// END: decoding //
//***************//
} else {
    exit("\n" . 'ERROR: The option flag "-p" accepts either "encode" or "decode".' . "\n\n");
}
