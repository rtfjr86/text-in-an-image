***************
* DESCRIPTION *
***************

This program can be used to embed a text string into an image or extract a text string from an image.
The input image is expected to be a JPEG file. The program will output a PNG file.
Text to be encoded can come from the command line, a text file, or the standard input stream.
The repository can be found at https://bitbucket.org/rtfjr86/text-in-an-image.

Created by: Richard T. Fields Jr.

**********
* ENCODE *
**********

Required Flags

 -f: This is the name of the message file to read from. This is only required in conjunction with the "-t file" value. (see "-t" option)

 -i: This is the image file that will be used to hide the message in.

 -m: This is used to enter a message on the command line. This is only required in conjunction with the "-t cmdln" value. (see "-t" option)

 -o: This this is the name of the image file to be output.

 -p: This is the operation to be performed. Valid options are either "encode" or "decode".

 -t: This is type of input that the message will be read from. Valid options are "stdin", "file", or "cmdln".
        "stdin" = standard input
        "file" = path to a file to read from
        "cmdln" = a string entered as a command line argument (see "-m" option)

**********
* DECODE *
**********

Required Flags

 -i: This is the image file that contains the message to be decoded.

 -p: This is the operation to be performed. Valid options are either "encode" or "decode".

***********
* SAMPLES *
***********

Sample Encode Text From The Command Line

    php secret.php -p encode -i smiley.jpg -o smiley.png -t cmdln -m "Hello World."

Sample Encode Text From A Text File

    php secret.php -p encode -i smiley.jpg -o smiley.png -t file -f message.txt

Sample Encode Text From Standard Input

    echo "This is a secret message." | php secret.php -p encode -i smiley.jpg -o smiley.png -t stdin

Sample Decode Text From An Image

    php secret.php -p decode -i smiley.png

*********
* Notes *
*********

 - "Hello World." has a binary length of "96" bits.
 - 96 in binary is "00000000000000000000000001100000"
 - In the fist pixel (bottom right), the least significant bits of the red, green, and blue
   values are set to the be binary length, beginning with the most significant bit.

...[rgb][rgb][rgb][rgb][rgb][rgb][rgb][rgb][rgb][rgb][rgb]
...[000][000][011][000][000][000][000][000][000][000][000]